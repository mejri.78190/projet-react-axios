import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Contact = () => {

function sayHello(){
    alert ("Hello World");
}

    return (
        <div>
            <Logo/>
            <Navigation/>

            <div>
            <button onClick={sayHello}>click</button>
            </div>

        </div>
    );
};

export default Contact;