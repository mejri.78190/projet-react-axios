import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Contact from './pages/Contact';
import Cinema from './pages/Cinema';
import Geographie from './pages/Geographie';
import Home from './pages/Home';
import Notfound from './pages/Notfound';

import React from 'react';
import Film from './pages/Film';

const App = () => {
  return (
    
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/contact' element={<Contact />}/>
        <Route path='/cinema' element={ <Cinema />}/>
        <Route path='/geographie' element={<Geographie />} />
        <Route path='/film' element={<Film />} />
        <Route path='*' element={<Notfound />} />

      </Routes>

    </BrowserRouter>

  );
};

export default App;

