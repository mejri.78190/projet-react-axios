import React from 'react';
import { NavLink } from 'react-router-dom';// le routage + le navlink 

const Navigation = () => {
    return (
        <nav className='navigation'>
        <ul>
    
            <NavLink  to="/" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Accueil</li>
            </NavLink>

            <NavLink to="/geographie" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Géographie</li>
            </NavLink>

            <NavLink to="/cinema" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Cinéma</li>
            </NavLink>

            <NavLink to="/film" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Film</li>
            </NavLink>

            <NavLink to="/contact" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Contact</li>
            </NavLink>
   


        </ul>
        </nav>
    );
};

export default Navigation;